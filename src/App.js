import ToggleColorMode from "./components/toggleColorMode";
import Views from "./components/views"
import UserContext from "./components/AccountContext";

function App() {
  return (
      <UserContext>
          <Views />
          <ToggleColorMode />
      </UserContext>
  );
}

export default App;
